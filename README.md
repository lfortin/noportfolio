# NoPortfolio

## English

### Getting started

Why on earth would a web developer call his portfolio project "NoPortfolio" ?

Because he does not believe in portfolios ... 

I actually believe that portfolios do not represent the developer you are. It is mostly a list of project you have worked on, wrapped (indeed) in some nice css (most of the time)

But guess what ? Every developer has worked on several projects, but some are private, and he cannot always present the best of them. Moreover, an online portfolio means that you need an online server, that uses power, for nothing most of the time ... And use power for nothing, that is definitively not the man I want to be, so I refuse to be that developer. I want to make the world a better place, and add more pollution to the one we are already suffocating of is not my aim.

### What now ?

So no, I will not do a portfolio. So how do I present my work and my skills ?
- By showing what I can do
- By respecting my beliefs
- By giving you some tools to make your projects easier

How can I do that ? Glad you asked ! Let me present you : my work. If you are here to see the project, you can see it [here on gitlab](https://gitlab.com/lfortin/react-seed). You can also see the roadmap : all the developed and furture features are [here on Trello](https://trello.com/b/WhxRfZwh/roadmap-react-seed). If you want to know my history and understand my choices, here is the presentation :

### React-seed

React-seed is an open-source project started in 2020 after my first react experience in Amsterdam, inspired by another project (angular-seed) from my previous experience in Brussels and forked from create-react-app.

The aim is to create an empty app, but that already contains all the tools you need to develop your own application. As soon as you want to create a blank project, you can fork it and start developping instead of configuring it.

Here is a list of the features that are already developed (many more will soon be added) :
- A router : navigate throw pages
- Redux : I love redux. Redux works great. Redux makes your life easier and nicer. But it is always a really bad moment to configure it, so I did it once and for all.
- Virtual lists : When working with big lists of items, your browser can have some difficult times. A virtual list removes this performance issue, so I added it by default (using Vurtuoso)

### History

- 2017-2019 : The inpiration

    When I was working in Brussels, most of my team's front end projects were developped with Angular. So they decided to create an empty project, a "seed" that was a pre-configured angular project.
    
    Along the time, some nice features were developed on each project, and every usefull ones were added to the seed to make it richer and richer. In 2019, I left the company and started to work with react, which I found way better than angular. That is the reason why my project is today in React and not in Angular

- 2020 : The need

    In 2019 I move to Amsterdam to discover a new work environment and learn to use React. I loved it, way easier to master and to use than Angular. After a few months (and a COVID crisis) I think it is time for me to move on again so I start to do some job interviews. Many of them ask me to do one thing : develop a front-end app, using a given API. Since I do not like to make the same job twice, I decied to create a first seed of a react app, that I would re-use for all the ephemeral projects I will have to do. And it worked, saved me hours and hours of boring and repetitive project's configuration.

- 2023 : The proof of concept

    In 2023 I got the responsability of 2 interns in my company. Since they do not know how to use React (nor Redux) a quick formation has been decided : create a small app, using the openweather API, in react, and redux. For that, I show them my seed, explained that they were free to use it, for their formation and their personnal projects, and explain how it works. After a few days of training, they felt really to work on a real react project and we included them in the developers team. Their feedback were good, I new that, if junior developer enjoyed the tool I made, I could make it even better

- 2024 : The evolution

    In fall 2023, I became Freelance and created my own company, working for clients, but also planning my own projects. I decided that it was time to upgrade my react seed so I made a roadmap and begon to improve it, as a living portfolio and a tool for my future projects. I try to make every decision to macth my beliefs and make it as sensible as possible. Here are a few of my choices and ideas :

    - Use gitlab and not github. Github is owned by Microsoft, and since I try not to use GAFAM products as much as I can (Fairphone with /e/os, Tutanota as mail client, ...), I moved to gitlab.
    - No online portfolio, instead create a project that shows what you can do. All the manager that I will work will are skilled enough to use a git clone and a yarn commands.
    - Make this project open-source : I love open source technologies, I use Linux (and make its promotion every day), I use them as much as I can, and I don't see why I would do a private project for a seed that everyone could use.
    - Use React. There is a battle between front-end frameworks (I know, react is a library), but I took several month to fully understand Angular while it only took me 2 weeks to master React. Today it is the most download project, and I believe there is a good reason for that (NB : I have never used Vue.js in any of my project, so I cannot compare with it)
    - Use Redux. That could be, at first, complicated to understand, but once you understood its behavior, that changes your life.
    - Use Typescript. I have been trained with C and Java, I am used to typed variables, and I do think this is a good practice to do so, so you don't use variables that are random object and that you need to log to know what is in it.
    - Use end-to-end test. I have never loved unit testing But I do think that end to end tests are very usefull and satisfying
    - Write unit tests : I don't like them, it doesn't mean I don't want to add them
    - Wrap it into a docker container. I think docker is an incredible technology that makes everything easier if you know how it works. I have learned it, and now I want to implement it in my own project
    - Many nice and adaptives features that will make my (and your) life more enjoyable in the future


The app's repository is available at this address [gitlab](https://gitlab.com/lfortin/react-seed). A star to the project would be appreciated !

You can see the roadmap at this address : [Trello link](https://trello.com/b/WhxRfZwh/roadmap-react-seed). Feel free to add your suggestions !


## 
## French

### Getting started

Pourquoi un développeur web voudrait appeler son projet portfolio "NoPortfolio" ?

Parce qu'il ne croit pas aux portfolios ...

Je pense qu'en réalité, les portfolios en représentent pas le développeur que vous êtes. C'est principalement une liste de projets sur lesquels vous avez travaillé, habillé (en effet) dans du beau css (la plupart du temps)

Mais devinez quoi ? Tous les développeurs ont travaillé sur plusieurs projets, mais certains sont privés, et ils ne peucvent pas tout le temps présenter les meilleurs d'entre eux. De plus, qui dit portfolio en ligne, dit serveur en ligne, qui utilise de l'énergie, pour rien la plupart du temps ... Et utiliser de l'énergie pour rien, ça n'est définitivement pas l'homme que je souhaite être, et refuse donc d'être ce développeur. Je veux faire du monde un meilleur endroit, et ajouter de la pollution à celle qui nous fait déjà suffoquer n'est pas mon objectif.

### What now ?

Donc non, je ne vais pas faire de portfolio. Comment vais donc présenter mon travail et mes compétences ?
- En montrant ce que je sais faire
- En respectant mes croyances
- En vous donnant des outils qui pourront faciliter vos projets.

Comment je fais ça ? Bonne question ! Laissez moi vous présenter : mon travail. Si vous êtes ici pour voir le budget, vous pouvez le voir [ici sur gitlab](https://gitlab.com/lfortin/react-seed). Vous pouvez aussi voir la roadmap : Toutes les features développées et à venir sont [ici sur Trello](https://trello.com/b/WhxRfZwh/roadmap-react-seed). Si vous voulez connaitre mon histoire et comprendre mes choix, voici la présentation :

### React-seed

React-seed est un projet open-source débuté en 2020 après ma première expérience react à Amsterdam, inspiré d'un autre projet (angular-seed) de mon expérience précédente à Bruxelles, et forké de create-react-app.

L'objectif est de créer une application vide, mais qui contient déjà tous les outils pour développer votre propre application. Dès que vous voulez créer un projet vierge, vous pouvez le forker et commencer à développer plutôt que le configurer.

Voici une liste de fonctionnalités déjà réalisées (beaucoup d'autres seront bientôt ajoutées) :
- Un router : naviguez de page en pages.
- Redux : J'aime redux. Redux ça marche bien. Redux rend votre vie plus belle et plus facile. Mais c'est toujours un mauvais moment de le configurer, donc je l'ai fait une bonne fois pour toutes.
- Listes virtuelles : En travaillant avec de grosses listes d'items, votre navigateur peut rencontrer des difficultés. Une liste virtuelle supprime ces problèmes de performance, j'en ai donc ajouté une par défaut (avec Virtuoso).

### History

- 2017-2019 : L'inpiration

    Quand je travaillais à Bruxelles, la plupart des projets front-end de mon équipe étaient développés avec Angular. Ils ont donc décidé de créer un projet vide, une "graine" qui serait un projet angular pré-configuré.

    Au fil du emps, certaines fonctionnalités intéressantes étaient développées sur chaque projet, et les plus utiles d'entre elles étaient ajoutées à la graine pour l'enrichir au fur et à mesure. En 2019, j'ai quitté l'entreprise et ai commencé à travailler avec react, que j'ai préféré à angular. C'est pourquoi mon projet est aujourd'hui en React et non en Angular.

- 2020 : Le besoin

    En 2019, je déménage à Amsterdam pour découvrir un nouvel environnement de travail et apprendre à utiliser React. J'ai adoré, bien plus simple à maitriser et utiliser qu'Angular. Après quelques mois (et une crise du COVID), il est temps pour moi de bouger à nouveau, et j'ai commencé à faire des entretiens d'embauche. Beaucoup d'entre-eux m'ont demandé une chose : développer une application front-end, en utilisant une API donnée. Comme je n'aime pas refaire le même travail deux fois, j'ai décidé de créer une première graine d'application react., que je réutiliserai pour tous ces projets éphémères que j'aurai à réaliser. Et cela a marché, me préservant de nombreuses heures de configuration ennuyante et répétitive.

- 2023 : Le POC

    En 2023 j'ai eu la responsabilité de 2 stagiaires dans mon entreprise. Vu qu'ils ne savaient pas uutiliser React (ni Redux), une formation rapide a été décidée : créer une petite application, utilisant l'API openweather, et redux. Pour cela, je leur ai montré ma seed, expliqué qu'ils pouvaient l'utiliser comme ils le souhaitaient, pour leur formation et leurs projets personnels, et expliqué comment cela marche. Après quelques jours de formation, ils se sentaient prêts pour travailler sur un vrai et nous les avons inclus dans l'équipe de développeur. Leurs retours étaient bons, je savais que, si un développeur junior avait pu profiter de l'outil que j'avais fait, je pouvais encore l'améliorer.

- 2024 : L'évolution

    À L'automne 2023, je suis devenu Freelance et ait créé ma propre entreprise, travaillant pour des clients, mais prévoyant mes propres projets. J'ai décidé qu'il était temps d'améliorer ma seed et ai donc fait une roadmap et ai commencé à l'améliorer comme un portfolio "live" et un outil pour mes futurs projets. J'essaye de faire que chacune de mes décisions respecte mes croyances et soit aussi sensée que possible. Voici quelques uns de mes choix et idées :

    - Utiliser gitlab et non github. Github est possédé par Mycrosoft, et vu qque j'essaye d'éviter autant que possible les produits des GAFAM (Fairphone avec /e/os, Tutanota comme client mail, ...), je suis allé sur gitlab.
    - Pas de portfolio en ligne. À la place, créer un projet qui montre ce que je sais faire. Tous les managers avec qui je vais travailler sont assez compétants pour utiliser les commandes git clone et yarn.
    - Rendre ce projet libre de droits : J'adore les technologies open-source, j'utilise Linux (et en fait la promotion tous les jours), je les utilise autant que possible, et ne vois pas pourquoi je devrais garder privé une seed que tout le monde pourrait utiliser.
    - Utiliser React. Il y a une guerre entre les frameworks front-end (je sais, react est une librairie), mais il m'a fallut plusieurs mois pour comprendre complètement Angular, quand 2 semaines m'ont suffit pour maîtriser React. Aujourd'hui c'est le projet le plus téléchargé, et je pense que c'est pour une bonne raison (NB: Je n'ai jamais utilisé Vue.js dans aucun de mes projets, donc je ne peux l'inclure dans ma comparaison).
    - Utiliser Redux. Ça peut être, au début, être complexe à comprendre, mais une fois que tu comprends son fonctionnement, cela change la vie.
    - Utiliser Typescript. J'ai été formé avec C et Java, je suis habitué aux variables typées, et je pense que c'est une bonne pratique de faire ainsi. Ainsi, vous n'utilisez pas de variables aléatoires et vous n'avez pas besoin d'afficher l'objet pour savoir ce qu'il contient.
    - Utiliser les tests end-to-end. Je n'ai jamais aimé les tests unitaires, mais je pense que les tests end-to-end sont très utiles et satisfaisants.
    - Écrire des tests unitaires : Je n'aime pas ça, mais cela ne veut pas dire que je ne veux pas en ajouter.
    - La mettre dans un container docker. Je pense que docker est une technologie incroyable qui rend tout plus facile si tu sais comment cela marche. Je l'ai apprise, et veux maintenant l'implémenter dans mon propre projet.
    - Plein de fonctionnailités chouettes et adaptatives qui rendront ma (et votre) vie plus appréciable à l'avenir.

Le repository de l'application est disponible à cette adresse [gitlab](https://gitlab.com/lfortin/react-seed). Une étoile sur le projet serait appréciée !

Vous pouvez voir la roadmap à cette url : [lien Trello](https://trello.com/b/WhxRfZwh/roadmap-react-seed). Vous êtes libres d'ajouter vos suggestions !
